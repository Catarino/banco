package model;

public class Conta {
	//Atributos
	protected double saldo;

	//Métodos
	public Conta(){
		
	}
	public double getSaldo(){
		return saldo;
	}
	public void deposito(double valor){
		this.saldo += valor;
	}
	public void saca(double valor){
		this.saldo -= valor;
	}
	public void atualiza(double taxa){
		this.saldo += this.saldo*taxa;
	}

}
