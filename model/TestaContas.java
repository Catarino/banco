package model;

public class TestaContas extends Conta{
	public static void main(String[] args){

		Conta c = new Conta();
		Conta cc = new ContaCorrente();
		Conta cp = new ContaPoupanca();

		c.deposito(1000);
		cc.deposito(1000);
		cp.deposito(1000);
		
		System.out.println("Saldo Conta: "+c.getSaldo());
		System.out.println("Saldo Conta Corrente: "+cc.getSaldo());
		System.out.println("Saldo Conta Poupanca: "+cp.getSaldo());
		
		c.atualiza(0.01);
		cc.atualiza(0.01);
		cp.atualiza(0.01);
		
		System.out.println("Saldo Conta: "+c.getSaldo());
		System.out.println("Saldo Conta Corrente: "+cc.getSaldo());
		System.out.println("Saldo Conta Poupanca: "+cp.getSaldo());
	}
}
