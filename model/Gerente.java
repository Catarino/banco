package model;

public class Gerente extends Funcionario{
	//Atributos
	private String senha;
	private int numeroFuncionarios;

	//M�todos
	public Gerente(){
		
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public int getNumeroFuncionarios() {
		return numeroFuncionarios;
	}
	public void setNumeroFuncionarios(int numeroFuncionarios) {
		this.numeroFuncionarios = numeroFuncionarios;
	}
	public static boolean autenticaSenha(String senha){
		if(senha == "1234"){
			System.out.println("Acesso Permitido!");
			return true;
		}
		else{
			System.out.println("Acesso Negado!");
			return false;
		}
	}
	//Main
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Gerente umGerente = new Gerente();
		umGerente.setNome("Joao");
		umGerente.setSenha("1234");
		umGerente.setSalario(1250.60);
		if(autenticaSenha(umGerente.getSenha())){
			System.out.println("Nome: "+umGerente.getNome()+"\n"+"Salario: "+umGerente.getSalario());
		}
			
	}

}
